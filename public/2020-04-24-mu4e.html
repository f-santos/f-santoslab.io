<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="alternate"
      type="application/rss+xml"
      href="https://f-santos.gitlab.io/rss.xml"
      title="RSS feed for https://f-santos.gitlab.io/"/>
<title>Reading and sending mails from within Emacs: a tutorial for mu4e</title>
<meta name="author" content="Frédéric Santos">
<meta name="referrer" content="no-referrer">
<link href= "solarized-light.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico">
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        displayAlign: "center",
        displayIndent: "0em",

        "HTML-CSS": { scale: 100,
                        linebreaks: { automatic: "false" },
                        webFont: "TeX"
                       },
        SVG: {scale: 100,
              linebreaks: { automatic: "false" },
              font: "TeX"},
        NativeMML: {scale: 100},
        TeX: { equationNumbers: {autoNumber: "AMS"},
               MultLineWidth: "85%",
               TagSide: "right",
               TagIndent: ".8em"
             }
});
</script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script></head>
<body>
<div id="preamble" class="status">
<div class="header"><a href="https://f-santos.gitlab.io/">Frédéric Santos' notebook</a><div class="slogan">Emacs and R tricks for anthropologists and archaeologists.</div><div class="sitelinks"><a href="https://f-santos.gitlab.io/"> Home </a>|<a href="https://f-santos.gitlab.io/about.html"> About </a>|<a href="https://f-santos.gitlab.io/rpackages.html"> R packages </a>|<a href="https://f-santos.gitlab.io/teaching.html"> Teaching (Master BGS) </a>| <div class="dropdown"><a href="https://f-santos.gitlab.io/tags.html" class="dropbtn"> Tags </a><div class="dropdown-content"><a href="https://f-santos.gitlab.io/tag-blog.html">blog</a><a href="https://f-santos.gitlab.io/tag-emacs.html">emacs</a><a href="https://f-santos.gitlab.io/tag-r.html">r</a><a href="https://f-santos.gitlab.io/tag-statistics.html">statistics</a><a href="https://f-santos.gitlab.io/tag-teaching.html">teaching</a></div></div></div></div></div>
<div id="content">
<div class="post-date">24 avril 2020</div><h1 class="post-title"><a href="https://f-santos.gitlab.io/2020-04-24-mu4e.html">Reading and sending mails from within Emacs: a tutorial for mu4e</a></h1>
<p>
There are several ways to handle (i.e., to read and send) all your emails directly from within Emacs. As you can read <a href="https://www.reddit.com/r/emacs/comments/ebite6/mu4e_vs_gnus_vs_notmuch_for_emacs_email/">on this reddit thread</a>, there are essentially three candidates: the built-in Emacs tools (<a href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Rmail.html">Rmail</a> and <a href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Sending-Mail.html#Sending-Mail">Gnus</a>), <a href="https://www.djcbsoftware.nl/code/mu/mu4e.html">mu4e</a> and <a href="https://www.emacswiki.org/emacs/NotMuch">NotMuch</a>. mu4e is a solution I find especially convenient and easy to use. In this post, I&rsquo;ll propose a quick tutorial and a reasonable mu4e configuration. Another tutorials out there are really well done, <a href="http://www.macs.hw.ac.uk/~rs46/posts/2014-01-13-mu4e-email-client.html">such as this one</a> (the present post is essentially a modified and extended version of this excellent tuto), but most of them are quite old, and do not necessarily play well with the latest versions of <a href="https://www.djcbsoftware.nl/code/mu/">mu</a>. This tutorial works for mu 1.4.x (and, I hope, later versions).
</p>

<div id="outline-container-org8b1d60b" class="outline-2">
<h2 id="org8b1d60b">What are mu and mu4e?</h2>
<div class="outline-text-2" id="text-org8b1d60b">
<p>
This is a three-story building :
</p>
<ol class="org-ol">
<li><i>&ldquo;mu is a tool for dealing with e-mail messages stored in the Maildir-format, on Unix-like systems&rdquo;</i> (from the official mu website)</li>
<li>mu4e is an Emacs-based client built on top on mu</li>
<li>Both of them need a tool such as <a href="http://isync.sourceforge.net/">isync</a>/mbsync to synchronise your emails between your remote email server and a local folder on your computer</li>
</ol>
</div>
</div>

<div id="outline-container-orgb1e4baa" class="outline-2">
<h2 id="orgb1e4baa"><span class="section-number-2">1</span> Install all the necessary stuff</h2>
<div class="outline-text-2" id="text-1">
<p>
In this section, I&rsquo;ll assume that you are a Linux user, but it should (?) be possible to find an équivalent of this for Mac OS or Windows.
</p>

<ol class="org-ol">
<li><p>
First, you should install SSL development libraries, that are called slightly differently from one Linux distro to another. For instance, Linux Manjaro users can execute:
</p>
<div class="org-src-container">
<pre class="src src-shell">sudo pacman -S openssl
</pre>
</div>
<p>
but this library may also be called <code>openssl-devel</code> or <code>libssl-dev</code> if you are using another distro.
</p></li>

<li><p>
Then, you can install mu and isync/mbsync. Although they are available on Git repositories and can be manually installed, using the package manager of your Linux distro is the easiest way. Manjaro users can execute the following commands:
</p>
<div class="org-src-container">
<pre class="src src-shell">pacaur -S mu
sudo pacman -S isync
</pre>
</div>
<p>
The <a href="https://www.djcbsoftware.nl/code/mu/mu4e/Installation.html">official manual of mu</a> gives further instruction for other Linux distros.
</p></li>
</ol>
</div>
</div>

<div id="outline-container-orgae96005" class="outline-2">
<h2 id="orgae96005"><span class="section-number-2">2</span> Configure isync/mbsync</h2>
<div class="outline-text-2" id="text-2">
<p>
You need to store your login information in an encrypted file on your computer so that your server can (safely) recognize you.
</p>
</div>

<div id="outline-container-org9ebc754" class="outline-3">
<h3 id="org9ebc754"><span class="section-number-3">2.1</span> Password info</h3>
<div class="outline-text-3" id="text-2-1">
<ol class="org-ol">
<li>In your <code>.emacs.d/</code> folder, create a file <code>.mbsyncpass</code> and simply write your password in it.</li>
<li><p>
This file needs to be encrypted, so that this private information can be safely stored on your computer. You can encrypt this file with <code>gpg2</code> by running the following command into the <code>.emacs.d/</code> folder:
</p>
<div class="org-src-container">
<pre class="src src-shell">gpg2 --output .mbsyncpass.gpg --symmetric .mbsyncpass
</pre>
</div>
<p>
A new encrypted file <code>.mbsyncpass.gpg</code> has been created in this folder.
</p></li>
<li><p>
But now, do not forget to remove the temporary file <code>.mbsyncpass</code> you had to create in step 1!
</p>
<div class="org-src-container">
<pre class="src src-shell">rm .mbsyncpass
</pre>
</div></li>
</ol>
</div>
</div>

<div id="outline-container-org26f5760" class="outline-3">
<h3 id="org26f5760"><span class="section-number-3">2.2</span> Mail account info</h3>
<div class="outline-text-3" id="text-2-2">
<ol class="org-ol">
<li><p>
Create a file <code>~/.authinfo</code> directly in the root of your home directory. In this file, you&rsquo;ll have to store your mail account info, by using in this template:
</p>
<pre class="example">
machine smtp.example.com login myname port 587 password mypassword
</pre>
<p>
Simply replace <code>smtp.example.com</code> by your SMTP server, <code>myname</code> by your user name, <code>mypassword</code> by your password, and keep the remaining characters unchanged.
</p></li>
<li><p>
<i>Let&rsquo;s do it again</i>: you&rsquo;ll also need to encrypt this file, since it contains your password. You can run:
</p>
<div class="org-src-container">
<pre class="src src-shell">gpg2 --output ~/.authinfo.gpg --symmetric ~/.authinfo
</pre>
</div>
<p>
to create an encrypted version of this file, and then simply remove the temporary file you had to create:
</p>
<div class="org-src-container">
<pre class="src src-shell">rm .authinfo
</pre>
</div></li>
</ol>
</div>
</div>

<div id="outline-container-org422ad99" class="outline-3">
<h3 id="org422ad99"><span class="section-number-3">2.3</span> Configuration file for isync/mbsync</h3>
<div class="outline-text-3" id="text-2-3">
<p>
You&rsquo;ll have to create a configuration file for isync. Let&rsquo;s create a file <code>.mbsyncrc</code> in your <code>.emacs.d/</code> folder. Here is a template you can fill in:
</p>
<pre class="example">
IMAPAccount nameaccount
Host your.imap.server
User yourname
PassCmd "gpg2 -q --for-your-eyes-only --no-tty -d ~/.emacs.d/.mbsyncpass.gpg"
Port 993
SSLType IMAPS
AuthMechs *
CertificateFile /etc/ssl/certs/ca-certificates.crt

IMAPStore nameaccount-remote
Account nameaccount

MaildirStore nameaccount-local
Path ~/email/mbsyncmail/
Inbox ~/email/mbsyncmail/INBOX
SubFolders Verbatim

Channel nameaccount
Master :nameaccount-remote:
Slave :nameaccount-local:
Patterns *
Create Slave
Sync All
Expunge None
SyncState *
</pre>

<p>
The <a href="http://isync.sourceforge.net/mbsync.html">mbsync documentation</a> is very complete and you should find easily the meaning of each parameter. But in particular, the following ones are important:
</p>
<ul class="org-ul">
<li><code>Path</code> is the name of the local folder mbsync will use your computer to store your emails. Here, I chose to store them in a folder called <code>/email/mysyncmail</code>.</li>
<li><code>Patterns *</code> means that I want to synchronise all the folders contained in my mailbox. If you want to synchronise only the folders INBOX, Sent and Trash (for instance), simply replace this line by <code>Patterns "Sent" "INBOX" "Trash"</code>.</li>
<li><code>PassCmd</code> is the command that mbsync must execute to retrieve your password. Here, I simply indicate that mbsync must decrypt the file <code>~/.emacs.d/.mbsyncpass.gpg</code> you created in section <a href="#org9ebc754">2.1</a>, in which you stored your (encrypted) password. In you chose a different location for this file, adapt this instruction accordingly in your <code>.mbbsyncrc</code> file.</li>
</ul>

<p>
Finally, run the following command (and adapt it: replace <code>nameaccount</code> by the value you chose as the name account in your <code>.mbsyncrc</code> file), so that this config file can take effect. All your emails will be downloaded from the server, and then indexed locally:
</p>
<div class="org-src-container">
<pre class="src src-shell">mbsync --config ~/.emacs.d/.mbsyncrc nameaccount
mu init --maildir=~/email/mbsyncmail/
mu index
</pre>
</div>

<p>
Now, you&rsquo;re ready to use mu4e!
</p>
</div>
</div>
</div>

<div id="outline-container-orgf606249" class="outline-2">
<h2 id="orgf606249"><span class="section-number-2">3</span> Basic configuration of mu4e</h2>
<div class="outline-text-2" id="text-3">
<p>
Here are some of the mu4e-related instructions I have in my Emacs <code>init.el</code> file. They are convenient for me, they may not be convenient for you. You may want to dig into <a href="https://www.djcbsoftware.nl/code/mu/mu4e/Example-configs.html">the mu4e manual</a> to find the variables you would like to customize in a way you like.
</p>

<ol class="org-ol">
<li><p>
The first thing to do is to ensure that mu4e can be found by Emacs. On my computer, mu4e has been installed in the folder <code>/usr/share/emacs/site-lisp/mu4e</code>. Therefore, I must add this folder to my <code>load-path</code>, and load mu4e:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Add mu4e to the load-path:</span>
(add-to-list 'load-path <span style="color: #2aa198;">"/usr/share/emacs/site-lisp/mu4e"</span>)
(<span style="color: #859900; font-weight: bold;">require</span> '<span style="color: #268bd2; font-weight: bold;">mu4e</span>)
</pre>
</div></li>

<li><p>
Configure some SMTP settings according to the information from tour mail provider. Modify the following template using your own information:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">SMTP settings:</span>
(<span style="color: #859900; font-weight: bold;">setq</span> send-mail-function 'smtpmail-send-it)    <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">should not be modified</span>
(<span style="color: #859900; font-weight: bold;">setq</span> smtpmail-smtp-server <span style="color: #2aa198;">"your.smtp.server"</span>) <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">host running SMTP server</span>
(<span style="color: #859900; font-weight: bold;">setq</span> smtpmail-smtp-service 587)               <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">SMTP service port number</span>
(<span style="color: #859900; font-weight: bold;">setq</span> smtpmail-stream-type 'starttls)          <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">type of SMTP connections to use</span>
</pre>
</div></li>

<li><p>
Specify the name of the draft, sent and trash folders (they must exist and begin by a <code>/</code>):
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Mail folders:</span>
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-drafts-folder <span style="color: #2aa198;">"/Drafts"</span>)
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-sent-folder   <span style="color: #2aa198;">"/Sent"</span>)
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-trash-folder  <span style="color: #2aa198;">"/Trash"</span>)
</pre>
</div></li>

<li><p>
Specify how mu4e should get and display your incoming emails:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">The command used to get your emails (adapt this line, see section 2.3):</span>
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-get-mail-command <span style="color: #2aa198;">"mbsync --config ~/.emacs.d/.mbsyncrc nameaccount"</span>)
<span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Further customization:</span>
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-html2text-command <span style="color: #2aa198;">"w3m -T text/html"</span> <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">how to hanfle html-formatted emails</span>
      mu4e-update-interval 300                  <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">seconds between each mail retrieval</span>
      mu4e-headers-auto-update t                <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">avoid to type `g' to update</span>
      mu4e-view-show-images t                   <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">show images in the view buffer</span>
      mu4e-compose-signature-auto-include nil   <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">I don't want a message signature</span>
      mu4e-use-fancy-chars t)                   <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">allow fancy icons for mail threads</span>
</pre>
</div></li>

<li><p>
Some possible other tweaks. For example, if you never want to include yourself when you choose &ldquo;reply to all&rdquo;, you can customize the variable `mu4e-compose-reply-ignore-address&rsquo;:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Do not reply to yourself:</span>
(<span style="color: #859900; font-weight: bold;">setq</span> mu4e-compose-reply-ignore-address '(<span style="color: #2aa198;">"no-?reply"</span> <span style="color: #2aa198;">"your.own@email.address"</span>))
</pre>
</div>
<p>
If you do not want to use <code>auto-fill-mode</code> when writing your emails:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Do not use auto-fill-mode for emails:</span>
(<span style="color: #859900; font-weight: bold;">defun</span> <span style="color: #268bd2;">auto-fill-mode-off</span> ()
  (auto-fill-mode 0))
(add-hook 'mu4e-compose-mode-hook 'auto-fill-mode-off)
</pre>
</div>
<p>
mu4e also allows to modify the expression used to introduce a quoted email. As a native french speaker, I chose this one:
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Modify the expression introducing a quoted email:</span>
(<span style="color: #859900; font-weight: bold;">setq</span> message-citation-line-function 'message-insert-formatted-citation-line)
(<span style="color: #859900; font-weight: bold;">setq</span> message-citation-line-format <span style="color: #2aa198;">"Le %Y-%m-%d &#224; %T %Z, %f a &#233;crit :\n"</span>)
</pre>
</div></li>

<li>It&rsquo;s up to you ! :)</li>
</ol>
</div>
</div>

<div id="outline-container-orge15dc8b" class="outline-2">
<h2 id="orge15dc8b"><span class="section-number-2">4</span> Launch mu4e</h2>
<div class="outline-text-2" id="text-4">
<p>
Simply execute <code>M-x mu4e</code> (or bind this to a convenient shortcut): you&rsquo;re done!
<img src="mu4e.png" alt="mu4e.png">
</p>

<p>
In the main <code>*mu4e-headers*</code> buffer, you just have to become familiar with some shortcuts:
</p>

<table>


<colgroup>
<col  class="org-left">

<col  class="org-left">
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Key</th>
<th scope="col" class="org-left">Action</th>
</tr>
</thead>
<tbody>
<tr>
<td class="org-left">C</td>
<td class="org-left">Compose a new email</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-left">R</td>
<td class="org-left">Reply to an email</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-left">F</td>
<td class="org-left">Forward an email</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-left">j</td>
<td class="org-left">Jump to a given folder</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-left">RET</td>
<td class="org-left">Open email in a new buffer</td>
</tr>
</tbody>
<tbody>
<tr>
<td class="org-left">C-c C-u</td>
<td class="org-left">Manually retrieve incoming emails from the server</td>
</tr>
</tbody>
</table>
</div>
</div>

<div id="outline-container-org91a504e" class="outline-2">
<h2 id="org91a504e"><span class="section-number-2">5</span> Get desktop alerts for new emails</h2>
<div class="outline-text-2" id="text-5">
<p>
Thanks to the setting <code>(setq mu4e-update-interval 300)</code>, mu4e is asked to retrieve emails every 300 seconds. But you&rsquo;re not really notified in a very clear way. Users migrating from an email client such as Thunderbird for example, may like to have pretty desktop alerts. A very nice Emacs package, <a href="https://github.com/iqbalansari/mu4e-alert">mu4e-alert</a>, allows this kind of notification.
</p>

<p>
This package is clearly documented, and easy to customize. I wanted something a bit different from the standard behavior, that I found sometimes a bit intrusive. Most often, when I start Emacs for the first time on the morning, checking my incoming emails is one of the first things I do, and I&rsquo;m happy to be notified when I have new emails. But in other contexts, when I need to focus and do not want to be disturbed, or simply when I&rsquo;m working in a train with a bad connection, I simply want Emacs to stop with all the email stuff (retrieval, notifications and so on). I wanted an &ldquo;on-demand&rdquo; behavior: either automatic and periodic retrieval of new emails with desktop alerts, or nothing at all.
</p>

<p>
The following instructions allow me to do that. When I start Emacs, it will first consider that it should not handle all the email stuff, and will not behave at all as an email client, unless I press <code>F2</code>. As soon as I press <code>F2</code>, and until the end of the Emacs session, Emacs will retrieve emails and display nice desktop alerts.
</p>

<p>
May you be interested by such a customization, here are the required instructions. (Warning: they make use of <a href="https://github.com/jwiegley/use-package">use-package</a>.)
</p>
<div class="org-src-container">
<pre class="src src-emacs-lisp"><span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Configure desktop notifs for incoming emails:</span>
(<span style="color: #859900; font-weight: bold;">use-package</span> <span style="color: #268bd2; font-weight: bold;">mu4e-alert</span>
  <span style="color: #657b83; font-weight: bold;">:ensure</span> t
  <span style="color: #657b83; font-weight: bold;">:init</span>
  (<span style="color: #859900; font-weight: bold;">defun</span> <span style="color: #268bd2;">perso--mu4e-notif</span> ()
    <span style="color: #2aa198;">"Display both mode line and desktop alerts for incoming new emails."</span>
    (<span style="color: #859900; font-weight: bold;">interactive</span>)
    (mu4e-update-mail-and-index 1)        <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">getting new emails is ran in the background</span>
    (mu4e-alert-enable-mode-line-display) <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">display new emails in mode-line</span>
    (mu4e-alert-enable-notifications))    <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">enable desktop notifications for new emails</span>
  (<span style="color: #859900; font-weight: bold;">defun</span> <span style="color: #268bd2;">perso--mu4e-refresh</span> ()
    <span style="color: #2aa198;">"Refresh emails every 300 seconds and display desktop alerts."</span>
    (<span style="color: #859900; font-weight: bold;">interactive</span>)
    (mu4e t)                            <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">start silently mu4e (mandatory for mu&gt;=1.3.8)</span>
    (run-with-timer 0 300 'perso--mu4e-notif))
  <span style="color: #657b83; font-weight: bold;">:after</span> mu4e
  <span style="color: #657b83; font-weight: bold;">:bind</span> (<span style="color: #2aa198;">"&lt;f2&gt;"</span> . perso--mu4e-refresh)  <span style="color: #93a1a1;">; </span><span style="color: #93a1a1;">F2 turns Emacs into a mail client</span>
  <span style="color: #657b83; font-weight: bold;">:config</span>
  <span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Mode line alerts:</span>
  (add-hook 'after-init-hook #'mu4e-alert-enable-mode-line-display)
  <span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Desktop alerts:</span>
  (mu4e-alert-set-default-style 'libnotify)
  (add-hook 'after-init-hook #'mu4e-alert-enable-notifications)
  <span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">Only notify for "interesting" (non-trashed) new emails:</span>
  (<span style="color: #859900; font-weight: bold;">setq</span> mu4e-alert-interesting-mail-query
        (concat
         <span style="color: #2aa198;">"flag:unread maildir:/INBOX"</span>
         <span style="color: #2aa198;">" AND NOT flag:trashed"</span>)))
</pre>
</div>
</div>
</div>
<div class="taglist"><a href="https://f-santos.gitlab.io/tags.html">Tags</a>: <a href="https://f-santos.gitlab.io/tag-emacs.html">emacs</a> </div></div>
<div id="postamble" class="status"><hr> <footer> <center> <a itemprop="sameAs" content="https://gitlab.com/f-santos" href="https://gitlab.com/f-santos" target="gitlab.widget" rel="me noopener noreferrer"> <img src="https://img.icons8.com/color/48/000000/gitlab.png" style="width:1em;margin-right:.5em;" alt="GitLab iD icon">GitLab</a> | <a itemprop="sameAs" content="https://orcid.org/0000-0003-1445-3871" href="https://orcid.org/0000-0003-1445-3871" target="orcid.widget" rel="me noopener noreferrer"> <img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon">ORCID</a></center>
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />All the material published in this blog is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</footer</div>
</body>
</html>
