<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="alternate"
      type="application/rss+xml"
      href="https://f-santos.gitlab.io/rss.xml"
      title="RSS feed for https://f-santos.gitlab.io/"/>
<title>How to know if an individual specimen may be a member of a given reference population?</title>
<meta name="author" content="Frédéric Santos">
<meta name="referrer" content="no-referrer">
<link href= "solarized-light.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico">
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        displayAlign: "center",
        displayIndent: "0em",

        "HTML-CSS": { scale: 100,
                        linebreaks: { automatic: "false" },
                        webFont: "TeX"
                       },
        SVG: {scale: 100,
              linebreaks: { automatic: "false" },
              font: "TeX"},
        NativeMML: {scale: 100},
        TeX: { equationNumbers: {autoNumber: "AMS"},
               MultLineWidth: "85%",
               TagSide: "right",
               TagIndent: ".8em"
             }
});
</script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script></head>
<body>
<div id="preamble" class="status">
<div class="header"><a href="https://f-santos.gitlab.io/">Frédéric Santos' notebook</a><div class="slogan">Emacs and R tricks for anthropologists and archaeologists.</div><div class="sitelinks"><a href="https://f-santos.gitlab.io/"> Home </a>|<a href="https://f-santos.gitlab.io/about.html"> About </a>|<a href="https://f-santos.gitlab.io/rpackages.html"> R packages </a>|<a href="https://f-santos.gitlab.io/teaching.html"> Teaching (Master BGS) </a>| <div class="dropdown"><a href="https://f-santos.gitlab.io/tags.html" class="dropbtn"> Tags </a><div class="dropdown-content"><a href="https://f-santos.gitlab.io/tag-blog.html">blog</a><a href="https://f-santos.gitlab.io/tag-emacs.html">emacs</a><a href="https://f-santos.gitlab.io/tag-r.html">r</a><a href="https://f-santos.gitlab.io/tag-statistics.html">statistics</a><a href="https://f-santos.gitlab.io/tag-teaching.html">teaching</a></div></div></div></div></div>
<div id="content">
<div class="post-date">15 avril 2020</div><h1 class="post-title"><a href="https://f-santos.gitlab.io/2020-04-15-typprob.html">How to know if an individual specimen may be a member of a given reference population?</a></h1>
<p>
Let&rsquo;s say you have one given specimen of unknown origin or species. Although you do not know precisely to which group this individual belongs, you have several hypotheses; e.g., you think that this individual might belong to populations A or B, but is certainly not a member of population C. In this post, we will illustrate some possible methods to assess the credibility of those hypotheses, depending on the data you were able to collect (or you are willing to use).
</p>
<ul class="org-ul">
<li>If you have only one continuous variable for both your unknown individual and your reference population sample, a simple yet efficient univariate method may be the so-called &ldquo;probabilistic distance&rdquo;.</li>
<li>If you have several continuous variables, one can think of various approaches. A basic one (frequently encountered within the literature in biological anthropology) makes use of principal component analyses and data ellipses. An alternative would be to use the concept of <i>typicality probability</i>.</li>
</ul>

<p>
Let&rsquo;s start with the simplest case.
</p>

<div id="outline-container-org315a868" class="outline-2">
<h2 id="org315a868"><span class="section-number-2">1</span> The univariate case</h2>
<div class="outline-text-2" id="text-1">
</div>
<div id="outline-container-orge766f8e" class="outline-3">
<h3 id="orge766f8e"><span class="section-number-3">1.1</span> The &ldquo;probabilistic distance&rdquo;</h3>
<div class="outline-text-3" id="text-1-1">
</div>
<div id="outline-container-orgdb0b748" class="outline-4">
<h4 id="orgdb0b748"><span class="section-number-4">1.1.1</span> Theory</h4>
<div class="outline-text-4" id="text-1-1-1">
<p>
The &ldquo;probabilistic distance&rdquo; (sometimes abbreviated <i>dpx</i>), or <i>distance probabiliste</i> in French, is basically a two sample <i>t</i>-test when one of the samples under comparison has only one individual. Within this framework, the null hypothesis \(\mathcal{H}_0\) might be rephrased as: &ldquo;the unknown individual is a member of the reference population&rdquo;. The use of this method was popularized by Bruno Maureille and coworkers; full theoretical details may be found in two of their publications [<a href="#scolan2012_NouveauxVestigesNeanderthaliens">4</a>,<a href="#maureille2001_DentsInferieuresNeandertalien">2</a>]. 
</p>

<p>
The probabilistic distance assumes that the reference population sample is normally distributed. In this case, the final result is simply a <i>p</i>-value for the null hypothesis. It may be interpreted as the probability to find in the underlying reference population an individual as extreme as, or even more extreme than, the target individual under study. Consequently, a very low probabilistic distance (let&rsquo;s say \(p = 0.0001\)) constitutes a strong evidence against the hypothesis that the unknown individual belongs to the reference population. Conversely, a probabilistic distance close to 1 means that the unknown individual is close to the average individual from the reference population, thus providing no evidence to discard the null hypothesis<sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup>.
</p>
</div>
</div>

<div id="outline-container-org7dc4027" class="outline-4">
<h4 id="org7dc4027"><span class="section-number-4">1.1.2</span> Practice with R</h4>
<div class="outline-text-4" id="text-1-1-2">
<p>
It is very easy to implement the formula of the probabilistic distance in a spreadsheet or in R. The detailed formulae can be found in [<a href="#scolan2012_NouveauxVestigesNeanderthaliens">4</a>]. However, the R package <a href="https://gitlab.com/f-santos/anthrostat">anthrostat</a> offers an R function, <code>dpx()</code>, to facilitate the computations out of the box. Here is an example of its use:
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(anthrostat)
<span style="color: #268bd2;">set.seed</span>(<span style="color: #b58900;">2020</span>) <span style="color: #93a1a1;"># </span><span style="color: #93a1a1;">to ensure replicability</span>
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Generate (at random) a reference population sample of 15 indiv.:</span>
ref <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">rnorm</span>(<span style="color: #b58900;">15</span>, mean = <span style="color: #b58900;">25</span>, sd = <span style="color: #b58900;">3</span>)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Distance probabiliste for an unknown individual x:</span>
<span style="color: #268bd2;">dpx</span>(x = <span style="color: #b58900;">20</span>, ref)
</pre>
</div>

<pre class="example">

[1] 0.1838366
</pre>


<p>
The probabilistic distance is approximately equal to 0.18. Thus, for an unknown individual with a value \(x = 20\), there is no strong evidence that this individual might not be a member of the reference population sample (composed of 15 individuals distributed according to \(\mathcal{N}(25, 9)\)).
</p>
</div>
</div>
</div>

<div id="outline-container-org11fdc1c" class="outline-3">
<h3 id="org11fdc1c"><span class="section-number-3">1.2</span> Alternatives</h3>
<div class="outline-text-3" id="text-1-2">
<p>
If the reference population sample is large enough, it might also be possible to apply any (robust) method of outlier detection to the whole data, i.e. the \(n+1\) values composed of the reference sample and the unknown individual. If the unknown individual is detected as an outlier within this sample, this may constitute an evidence against the hypothesis that the unknown individual is a member of the reference sample.
</p>
</div>
</div>
</div>

<div id="outline-container-org92531fa" class="outline-2">
<h2 id="org92531fa"><span class="section-number-2">2</span> The multivariate case with one single reference sample</h2>
<div class="outline-text-2" id="text-2">
<p>
Let&rsquo;s suppose now that we have several variables \(X_1, X_2, \cdots, X_p\) involved in the comparison of the unknown individual and the reference population sample.
</p>
</div>

<div id="outline-container-orgd114a05" class="outline-3">
<h3 id="orgd114a05"><span class="section-number-3">2.1</span> PCA and data ellipses</h3>
<div class="outline-text-3" id="text-2-1">
<p>
A first possible approach would be to perform a PCA on the whole dataset<sup><a id="fnr.2" class="footref" href="#fn.2">2</a></sup>. For this example, we will use an example from the <a href="http://web.utk.edu/~auerbach/GOLD.htm">Goldman Data Set, freely available online</a> [<a href="#auerbach2004_HumanBodyMass">1</a>]. Our reference population sample will be composed of 29 male individuals from the Indian Knoll population sample (US-KT, 5500&#x2013;3700 BP), and our target individual will be a male individual from Tigara population sample (US-AK, 750&#x2013;300 BP). Thus, we hope to demonstrate that the target individual is <i>not</i> a member of the reference sample.
</p>

<p>
Six variables of interest will be considered here (various femoral, humeral and radial bone measurements). Their corresponding short codes in the Goldman Data Set are LFML, LHML, RTML, LHMLD, LFAPD, LFMLD respectively.
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(dplyr)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Load dataset:</span>
gds <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">read.csv</span>(<span style="color: #2aa198;">"http://web.utk.edu/~auerbach/Goldman.csv"</span>,
                dec = <span style="color: #2aa198;">"."</span>, na.strings = <span style="color: #2aa198;">""</span>,
                stringsAsFactors = <span style="color: #b58900;">TRUE</span>,
                fileEncoding = <span style="color: #2aa198;">"macintosh"</span>)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Select the variables of interest and keep only male individuals:</span>
gds <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">subset</span>(gds, Sex == <span style="color: #2aa198;">"0"</span>)
gds <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">select</span>(gds, NOTE, LFML, LHML, RTML, LHMLD, LFAPD, LFMLD)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Select a reference sample and a target individual:</span>
ref <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">subset</span>(gds, NOTE == <span style="color: #2aa198;">"Indian Knoll"</span>)
target <span style="color: #268bd2; font-weight: bold;">&lt;-</span> gds[<span style="color: #b58900;">1</span>, ]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Merge them in one dataframe:</span>
dat <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">rbind</span>(target, ref)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Drop unused factor levels:</span>
dat <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">droplevels</span>(dat)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Summarize data:</span>
<span style="color: #268bd2;">summary</span>(dat)
</pre>
</div>

<pre class="example">

                      NOTE         LFML            LHML            RTML      
 Indian Knoll           :29   Min.   :404.0   Min.   :292.0   Min.   :337.5  
 Tigara - Point Hope, AK: 1   1st Qu.:424.8   1st Qu.:305.5   1st Qu.:355.5  
                              Median :441.0   Median :312.5   Median :367.0  
                              Mean   :439.9   Mean   :318.0   Mean   :365.8  
                              3rd Qu.:457.8   3rd Qu.:327.5   3rd Qu.:373.0  
                              Max.   :474.0   Max.   :354.0   Max.   :401.0  
                                              NA's   :1       NA's   :3      
     LHMLD           LFAPD           LFMLD      
 Min.   :16.09   Min.   :23.92   Min.   :20.66  
 1st Qu.:18.47   1st Qu.:25.66   1st Qu.:22.55  
 Median :19.10   Median :26.68   Median :23.58  
 Mean   :19.16   Mean   :26.81   Mean   :23.91  
 3rd Qu.:20.29   3rd Qu.:28.09   3rd Qu.:24.59  
 Max.   :24.30   Max.   :31.39   Max.   :29.77
</pre>

<p>
There are only very few missing values here, so that we can simply ignore them and use the default method, i.e. the replacement by the column means<sup><a id="fnr.3" class="footref" href="#fn.3">3</a></sup>, and perform a PCA:
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(FactoMineR)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Compute (silently) the PCA:</span>
res_pca <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">PCA</span>(dat, quali.sup = <span style="color: #b58900;">1</span>, graph = <span style="color: #b58900;">FALSE</span>)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Display a PCA plot:</span>
<span style="color: #268bd2;">plot</span>(res_pca, habillage = <span style="color: #b58900;">1</span>, graph.type = <span style="color: #2aa198;">"classic"</span>)
</pre>
</div>


<figure id="orgef1c410">
<img src="2020-04-15-PCA.png" alt="2020-04-15-PCA.png">

<figcaption><span class="figure-number">Figure 1: </span>First two principal axes of the PCA.</figcaption>
</figure>

<p>
As it can be seen on figure <a href="#orgef1c410">1</a>, the null hypothesis that the target individual (in red) belongs to the reference population sample (in black) is rather unlikely (well, in this toy example, we know that it&rsquo;s <i>really</i> false!). Adding a 95% data ellipse around the reference population sample is a reasonable way to better visualize this incompatibility:
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(car)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Display a PCA plot:</span>
<span style="color: #268bd2;">plot</span>(res_pca, habillage = <span style="color: #b58900;">1</span>, graph.type = <span style="color: #2aa198;">"classic"</span>)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Extract the principal coordinates of the reference sample</span>
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">(we remove the first line, since it corresponds to the target individual):</span>
coor <span style="color: #268bd2; font-weight: bold;">&lt;-</span> res_pca$ind$coor[-<span style="color: #b58900;">1</span>, ]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Add a 95% data ellipse:</span>
<span style="color: #268bd2;">dataEllipse</span>(x = coor[, <span style="color: #b58900;">1</span>], <span style="color: #93a1a1;"># </span><span style="color: #93a1a1;">PC1</span>
            y = coor[, <span style="color: #b58900;">2</span>], <span style="color: #93a1a1;"># </span><span style="color: #93a1a1;">PC2</span>
            levels = <span style="color: #b58900;">0.95</span>, <span style="color: #93a1a1;"># </span><span style="color: #93a1a1;">95% ellipse</span>
            col = <span style="color: #2aa198;">"black"</span>, add = <span style="color: #b58900;">TRUE</span>)
</pre>
</div>


<figure id="org73fc2b8">
<img src="2020-04-15-ellipses.png" alt="2020-04-15-ellipses.png">

<figcaption><span class="figure-number">Figure 2: </span>95% data ellipse for the reference population sample of Indian Knoll, computed and superimposed on the first two principal axes.</figcaption>
</figure>

<p>
The figure <a href="#org73fc2b8">2</a> is eloquent, and brings an additional proof against the hypothesis that the target individual might be a member of Indian Knoll population sample: it is clearly not within the region of the 95% &ldquo;most credible&rdquo; individuals for this population sample. Some remarks, however:
</p>
<ol class="org-ol">
<li>This discussion must take into account the percentage of variance captured by the first two principal components (here, it is large enough: it is not mandatory to inspect the following principal components).</li>
<li>You can try with other confidence levels in order to get even an stronger evidence!</li>
<li>It is not sufficient to say that an individual may not be part of a reference sample: it is important to precise <i>why</i> (i.e., which variables or combination of variables observed on this individual are really unusual). Inspecting the correlation circle of the PCA should be instructive; applying univariate methods (as exposed in the previous section) on well-chosen variables should also help.</li>
</ol>
</div>

<div id="outline-container-org2272c45" class="outline-4">
<h4 id="org2272c45"><span class="section-number-4">2.1.1</span> Note: a fast alternative with the R package <code>factoextra</code></h4>
<div class="outline-text-4" id="text-2-1-1">
<p>
Producing manually the output in Figure <a href="#org73fc2b8">2</a> is quite cumbersome. It may be easier to use the R package <code>factoextra</code>. Using its function <code>fviz_pca_ind()</code>, one can directly display a pretty PCA plot with data ellipses, directly from the results returned by the <code>PCA()</code> function.
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(factoextra)
<span style="color: #268bd2;">fviz_pca_ind</span>(res_pca, habillage = <span style="color: #b58900;">1</span>, addEllipses = <span style="color: #b58900;">TRUE</span>)
</pre>
</div>


<figure>
<img src="fviz_pca.png" alt="fviz_pca.png">

<figcaption><span class="figure-number">Figure 3: </span>A similar output using the R package <code>factoextra</code>.</figcaption>
</figure>
</div>
</div>
</div>

<div id="outline-container-org8c603cb" class="outline-3">
<h3 id="org8c603cb"><span class="section-number-3">2.2</span> Alternatives</h3>
<div class="outline-text-3" id="text-2-2">
<p>
As for the univariate case, it might be possible to apply any multivariate outlier detection method to the whole dataset, including both the reference sample and the target individual.
</p>
</div>
</div>
</div>

<div id="outline-container-org0e57afd" class="outline-2">
<h2 id="org0e57afd"><span class="section-number-2">3</span> The multivariate case with several one or several reference samples : typicality probability</h2>
<div class="outline-text-2" id="text-3">
</div>
<div id="outline-container-org90d064d" class="outline-3">
<h3 id="org90d064d"><span class="section-number-3">3.1</span> Philosophy</h3>
<div class="outline-text-3" id="text-3-1">
<p>
The concept of <i>typicality probability</i> allows to get quantitative information about the hypothesis that an individual belongs to a reference population [<a href="#mizoguchi2011_TypicalityProbabilitiesLate">3</a>,<a href="#wilson1981_ComparingFossilSpecimensa">5</a>], in a way that differs from other methods. This method is particularly useful if there are several population samples as candidates: does the unknown individual belong to population A or population B ?
</p>

<p>
If the question can really be formulated like this, a common way to solve the problem is to apply a linear discriminant analysis. This will provide posterior probabilities \(p_A, p_B\) for the alternative &ldquo;the individual comes from population A / population B&rdquo;. Here, those two hypotheses are mutually exclusive and one of them is necessarily true, so that \(p_A + p_B = 1\).
</p>

<p>
This framework may be inconvenient in many situations: it makes sense only when there are no other possible reference population for the unknown individual: it is strictly impossible that the individual belong to a third population C. In concrete applications, this cannot always be completely excluded. The goal would be to get a quantitative assessment of the hypotheses &ldquo;the individual may belong to pop A&rdquo; / &ldquo;the individual may belong to pop B&rdquo;, taking into account that:
</p>
<ul class="org-ul">
<li>the individual may be incompatible with both populations A and B;</li>
<li>the measurements of the individual may be perfectly compatible with <i>both</i> A and B (so that none of the two hypotheses can really be discarded).</li>
</ul>
<p>
In other words, the respective credibility of both hypotheses (\(p_A, p_B\)) will not necessarily sum up to 1.
</p>

<p>
The concept of typicality probability offers an answer to this problem. It might be seen as a multivariate version of the probabilistic distance.
</p>
</div>
</div>

<div id="outline-container-org94589a8" class="outline-3">
<h3 id="org94589a8"><span class="section-number-3">3.2</span> Mathematical details</h3>
<div class="outline-text-3" id="text-3-2">
<p>
How to compute the typicality probability for a given individual \(x\) and a reference sample? Full details can be found in the seminal article from Wilson [<a href="#wilson1981_ComparingFossilSpecimensa">5</a>]. Here is a short summary for the case where an individual is to be compared to one single reference sample:
</p>
<ol class="org-ol">
<li>It is assumed that the data within each group follow a multivariate normal distribution.</li>
<li>Compute the (squared) Mahalanobis distance between \(x\) and the centroid of the reference sample \(m\):
\[ D^2 = (x - m)' V^{-1} (x - m)\]
where \(V\) is the covariance matrix of the reference sample.</li>
<li>It can be proved that, if \(p\) is the number of variables and \(n\) the number of individuals in the reference sample:
\[f_\text{obs} = \frac{n(n-p)}{p(n-1)(n+1)} D^2 \sim \mathcal{F}(p, n-p) \]</li>
<li>Thus, the typicality probability is defined as \(1 - F(f_\text{obs})\), where \(F\) is the distribution function of a Fisher distribution with \(p\) and \(n-p\) degrees of freedom.</li>
</ol>

<p>
From its definition, we can see that the typicality probability is the probability to find within the reference population an individual as extreme as, or even more extreme than, the target individual.
</p>
</div>
</div>

<div id="outline-container-org6a1903d" class="outline-3">
<h3 id="org6a1903d"><span class="section-number-3">3.3</span> An example with R</h3>
<div class="outline-text-3" id="text-3-3">
<p>
Let&rsquo;s retrieve the example from section <a href="#orgd114a05">2.1</a>, but we&rsquo;ll add a second reference population sample: the foragers from Ipituaq (US-AK, 1500&#x2013;1100 BP). One could expect the target individual to be closer to Ipituaq population than Indian Knoll population, but the target individual might also be far from the two populations. 
</p>

<p>
First, we build a new dataset:
</p>
<div class="org-src-container">
<pre class="src src-R"><span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Add a second population sample from the Goldman Data Set:</span>
dat <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">rbind</span>(dat, <span style="color: #268bd2;">subset</span>(gds, NOTE == <span style="color: #2aa198;">"Ipituaq - Point Hope, AK"</span>))
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Drop unused factor levels, and remove all missing values:</span>
dat <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">droplevels</span>(dat)
dat <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">na.omit</span>(dat)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Summarize the data:</span>
<span style="color: #268bd2;">summary</span>(dat)
</pre>
</div>

<pre class="example">

                       NOTE         LFML            LHML            RTML      
 Indian Knoll            :25   Min.   :385.0   Min.   :275.0   Min.   :305.0  
 Tigara - Point Hope, AK : 1   1st Qu.:407.5   1st Qu.:301.0   1st Qu.:337.5  
 Ipituaq - Point Hope, AK:15   Median :429.0   Median :308.0   Median :360.0  
                               Mean   :429.3   Mean   :310.4   Mean   :354.2  
                               3rd Qu.:451.0   3rd Qu.:321.5   3rd Qu.:371.5  
                               Max.   :513.5   Max.   :352.5   Max.   :401.0  
     LHMLD           LFAPD           LFMLD      
 Min.   :16.09   Min.   :23.92   Min.   :20.66  
 1st Qu.:18.52   1st Qu.:26.06   1st Qu.:23.03  
 Median :19.53   Median :27.21   Median :24.64  
 Mean   :20.10   Mean   :27.45   Mean   :25.16  
 3rd Qu.:21.36   3rd Qu.:28.85   3rd Qu.:27.10  
 Max.   :25.91   Max.   :31.71   Max.   :29.96
</pre>

<p>
The R function <code>typprobClass()</code> from the R package <code>Morpho</code> can do what we want: calculate the typicality probability of a given individual <code>x</code> for several <code>groups</code>.
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(Morpho)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Isolate the target individual, which is in the first row:</span>
indiv <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">as.vector</span>(dat[<span style="color: #b58900;">1</span>, -<span style="color: #b58900;">1</span>]) <span style="color: #93a1a1;"># </span><span style="color: #93a1a1;">do not consider the first column here</span>
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Isolate the numeric data that constitute the reference sample,</span>
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">i.e. all the dataframe except the first row (target indiv)</span>
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">and the first column (grouping factor):</span>
ref <span style="color: #268bd2; font-weight: bold;">&lt;-</span> dat[-<span style="color: #b58900;">1</span>, -<span style="color: #b58900;">1</span>]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">For a greater ease of use below, keep apart the grouping factor:</span>
pop <span style="color: #268bd2; font-weight: bold;">&lt;-</span> dat[-<span style="color: #b58900;">1</span>, <span style="color: #2aa198;">"NOTE"</span>]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Compute typicality probabilities:</span>
<span style="color: #268bd2;">typprobClass</span>(x = indiv, data = ref, groups = pop,
             method = <span style="color: #2aa198;">"wilson"</span>)$probs
</pre>
</div>

<pre class="example">

  Indian Knoll Ipituaq - Point Hope, AK
1  0.001216739                0.1980937
</pre>


<p>
This suggests that, based on those six bone measurements, the target individual is certainly not a member of Indian Knoll (with a typicality probability as low as 0.001). However, his bone measurements might be compatible with those usually observed within the population of Ipituaq, since the typicality probability associated to this population sample is approximately 0.20.
</p>
</div>
</div>
</div>

<div id="bibliography">
<h2>References</h2>

</div>
<table>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="auerbach2004_HumanBodyMass">1</a>]
</td>
<td class="bibtexitem">
Benjamin&nbsp;M. Auerbach and Christopher&nbsp;B. Ruff.
 Human body mass estimation: A comparison of morphometric and
  mechanical methods.
 <em>American Journal of Physical Anthropology</em>, 125(4):331--342,
  2004.
[&nbsp;<a href="http://dx.doi.org/10.1002/ajpa.20032">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="maureille2001_DentsInferieuresNeandertalien">2</a>]
</td>
<td class="bibtexitem">
Bruno Maureille, H&eacute;l&egrave;ne Rougier, Francis Hou&euml;t, and Bernard
  Vandermeersch.
 Les dents inf&eacute;rieures du n&eacute;andertalien Regourdou 1 (site de
  Regourdou, commune de Montignac, Dordogne) : analyses m&eacute;triques et
  comparatives.
 <em>PALEO. Revue d'arch&eacute;ologie pr&eacute;historique</em>,
  (13):183--200, 2001.
 <a href="http://journals.openedition.org/paleo/1043">http://journals.openedition.org/paleo/1043</a>.

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="mizoguchi2011_TypicalityProbabilitiesLate">3</a>]
</td>
<td class="bibtexitem">
Yuji Mizoguchi.
 Typicality probabilities of Late Pleistocene human fossils from
  East Asia, Southeast Asia, and Australia: Implications for the
  Jomon population in Japan.
 <em>Anthropological Science</em>, 119(2):99--111, 2011.
[&nbsp;<a href="http://dx.doi.org/10.1537/ase.090330">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="scolan2012_NouveauxVestigesNeanderthaliens">4</a>]
</td>
<td class="bibtexitem">
H.&nbsp;Scolan, F.&nbsp;Santos, A.&nbsp;M. Tillier, B.&nbsp;Maureille, and A.&nbsp;Quintard.
 Des nouveaux vestiges n&eacute;anderthaliens &agrave; Las P&eacute;l&eacute;nos
  (Monsempron-Libos, Lot-et-Garonne, France).
 <em>Bulletins et m&eacute;moires de la Soci&eacute;t&eacute; d'anthropologie
  de Paris</em>, 24(1-2):69--95, 2012.
[&nbsp;<a href="http://dx.doi.org/10.1007/s13219-011-0047-x">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="wilson1981_ComparingFossilSpecimensa">5</a>]
</td>
<td class="bibtexitem">
S.&nbsp;R. Wilson.
 On comparing fossil specimens with population samples.
 <em>Journal of Human Evolution</em>, 10(3):207--214, 1981.
[&nbsp;<a href="http://dx.doi.org/10.1016/S0047-2484(81)80059-0">DOI</a>&nbsp;]

</td>
</tr>
</table>
<div id="footnotes">
<h2 class="footnotes">Footnotes: </h2>
<div id="text-footnotes">

<div class="footdef"><sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <div class="footpara"><p class="footpara">In this case, one must not conclude that the unknown individual <i>is</i> a member of the reference population! It is simply an hypothesis compatible with the observed data, among other credible hypotheses.</p></div></div>

<div class="footdef"><sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <div class="footpara"><p class="footpara">This supposes that there is a moderate amount of missing values in the reference sample.</p></div></div>

<div class="footdef"><sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <div class="footpara"><p class="footpara">In real life applications, applying missing values imputation methods would be preferable all the same.</p></div></div>


</div>
</div><div class="taglist"><a href="https://f-santos.gitlab.io/tags.html">Tags</a>: <a href="https://f-santos.gitlab.io/tag-r.html">R</a> <a href="https://f-santos.gitlab.io/tag-statistics.html">statistics</a> </div></div>
<div id="postamble" class="status"><hr> <footer> <center> <a itemprop="sameAs" content="https://gitlab.com/f-santos" href="https://gitlab.com/f-santos" target="gitlab.widget" rel="me noopener noreferrer"> <img src="https://img.icons8.com/color/48/000000/gitlab.png" style="width:1em;margin-right:.5em;" alt="GitLab iD icon">GitLab</a> | <a itemprop="sameAs" content="https://orcid.org/0000-0003-1445-3871" href="https://orcid.org/0000-0003-1445-3871" target="orcid.widget" rel="me noopener noreferrer"> <img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon">ORCID</a></center>
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />All the material published in this blog is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</footer</div>
</body>
</html>
