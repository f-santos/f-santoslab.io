<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="alternate"
      type="application/rss+xml"
      href="https://f-santos.gitlab.io/rss.xml"
      title="RSS feed for https://f-santos.gitlab.io/"/>
<title>Comment boxes for R scripts within Emacs</title>
<meta name="author" content="Frédéric Santos">
<meta name="referrer" content="no-referrer">
<link href= "solarized-light.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico">
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        displayAlign: "center",
        displayIndent: "0em",

        "HTML-CSS": { scale: 100,
                        linebreaks: { automatic: "false" },
                        webFont: "TeX"
                       },
        SVG: {scale: 100,
              linebreaks: { automatic: "false" },
              font: "TeX"},
        NativeMML: {scale: 100},
        TeX: { equationNumbers: {autoNumber: "AMS"},
               MultLineWidth: "85%",
               TagSide: "right",
               TagIndent: ".8em"
             }
});
</script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script></head>
<body>
<div id="preamble" class="status">
<div class="header"><a href="https://f-santos.gitlab.io/">Frédéric Santos' notebook</a><div class="slogan">Emacs and R tricks for anthropologists and archaeologists.</div><div class="sitelinks"><a href="https://f-santos.gitlab.io/"> Home </a>|<a href="https://f-santos.gitlab.io/about.html"> About </a>|<a href="https://f-santos.gitlab.io/rpackages.html"> R packages </a>|<a href="https://f-santos.gitlab.io/teaching.html"> Teaching (Master BGS) </a>| <div class="dropdown"><a href="https://f-santos.gitlab.io/tags.html" class="dropbtn"> Tags </a><div class="dropdown-content"></div></div></div></div></div>
<div id="content">
<div class="post-date">06 avril 2020</div><h1 class="post-title"><a href="https://f-santos.gitlab.io/2020-04-06-comment-boxes.html">Comment boxes for R scripts within Emacs</a></h1>
<p>
Some people like to insert nice comment boxes in their R scripts. Using Emacs and <a href="https://ess.r-project.org/">ESS</a>, this is easy to achieve thanks to the lisp function <code>comment-box</code>.
</p>

<div id="outline-container-org8da2a5d" class="outline-2">
<h2 id="org8da2a5d">A first example</h2>
<div class="outline-text-2" id="text-org8da2a5d">
<p>
Without any customization, here is an example of how <code>comment-box</code> can help you:
</p>


<figure>
<img src="./2020-04-06_simple-comment-box.gif" alt="2020-04-06_simple-comment-box.gif">

</figure>

<p>
By default, <code>comment-box</code> inserts a comment box around the desired region; and the box width corresponds to the region width.
</p>

<p>
Some additional customization may be useful.
</p>
</div>
</div>

<div id="outline-container-org20220ee" class="outline-2">
<h2 id="org20220ee">A possible keybinding</h2>
<div class="outline-text-2" id="text-org20220ee">
<p>
First, it would be convenient to define a keybinding for this function : <code>M-x comment-box</code> is quite a long instruction (and you have also to set the region manually, which adds another operation).
</p>
</div>
</div>

<div id="outline-container-orgef224fb" class="outline-2">
<h2 id="orgef224fb">Automatically adjust the number of # in the left margin of the comment box</h2>
<div class="outline-text-2" id="text-orgef224fb">
<p>
ESS handles comments in a particular way, according to the number of <code>#</code> that introduce them. According to <a href="http://ess.r-project.org/Manual/ess.html#Indenting">the manual</a>:
</p>
<blockquote>
<p>
By default, comments beginning with ‘###’ are aligned to the beginning of the line. Comments beginning with ‘##’ are aligned to the current level of indentation for the block containing the comment. Finally, comments beginning with ‘#’ are aligned to a column on the right 
</p>
</blockquote>

<p>
This behavior may be modified, but I find it really useful (and natural since it is consistent with Lisp conventions).
</p>

<p>
By default, <code>comment-box</code> inserts two <code>#</code> in the left margin of the comment box, so that the box may be indented depending on its position within the R script. But you may want a simple rule such as:
</p>
<ul class="org-ul">
<li>if the comment box corresponds to a 0-level of indentation, then it should start with three <code>#</code></li>
<li>otherwise, the comment box should start with two <code>#</code> only, and will be indented accordingly</li>
</ul>

<p>
This allows to distinguish different levels of information within the R script. Of course, you can provide a prefix argument to the <code>comment-box</code> function to adjust manually the number of <code>#</code> that are required, but this is quite cumbersome. A piece of Lisp code that handles that automatically is given below.
</p>
</div>
</div>

<div id="outline-container-orgd444bf3" class="outline-2">
<h2 id="orgd444bf3">A proposition of Lisp code</h2>
<div class="outline-text-2" id="text-orgd444bf3">
<p>
The following Emacs Lisp code might help.
</p>

<div class="org-src-container">
<pre class="src src-emacs-lisp">(<span style="color: #859900; font-weight: bold;">defun</span> <span style="color: #268bd2;">ess-r-comment-box-line</span> ()
  <span style="color: #2aa198;">"Insert a comment box around the text of the current line of an R script.</span>
<span style="color: #2aa198;">If the current line indentation is 0, the comment box begins with ###.</span>
<span style="color: #2aa198;">Otherwise, it begins with ## and is indented accordingly."</span>
  (<span style="color: #859900; font-weight: bold;">interactive</span>)
  (<span style="color: #859900; font-weight: bold;">save-excursion</span>
    (<span style="color: #859900; font-weight: bold;">let</span> ((beg (<span style="color: #859900; font-weight: bold;">progn</span> (back-to-indentation)
                      (point)))
          (end (line-end-position)))
      (comment-box beg end
                   (<span style="color: #859900; font-weight: bold;">if</span> (&gt; (current-indentation) 0)
                       1
                     2)))))

<span style="color: #93a1a1;">;; </span><span style="color: #93a1a1;">A keybinding specific to ESS-R mode:</span>
(add-hook 'ess-r-mode-hook
          '(lambda ()
             (local-set-key (kbd <span style="color: #2aa198;">"\C-cb"</span>) #'ess-r-comment-box-line)))
</pre>
</div>

<p>
This code allows three things to facilitate and standardize the use of comment boxes into an R script:
</p>
<ol class="org-ol">
<li>there is no need to set the region anymore: the comment box is inserted around the text of the current line, automatically</li>
<li>the number is <code>#</code> is adjusted according to the level of identation</li>
<li>the keybinding <code>C-c b</code> is proposed to trigger the function in ESS-R mode.</li>
</ol>

<p>
Here is an example of its use:
</p>


<figure>
<img src="./2020-04-06_improved-comment-box.gif" alt="2020-04-06_improved-comment-box.gif">

</figure>

<p>
Of course, all this is only an illustration of a possible use and customization of <code>comment-box</code>, and the present example is mainly a matter of personal taste.
</p>

<p>
Happy comment boxing!
</p>
</div>
</div>
<div class="taglist"><a href="https://f-santos.gitlab.io/tags.html">Tags</a>: <a href="https://f-santos.gitlab.io/tag-emacs.html">emacs</a> <a href="https://f-santos.gitlab.io/tag-r.html">R</a> </div></div>
<div id="postamble" class="status"><hr> <footer> <center> <a itemprop="sameAs" content="https://gitlab.com/f-santos" href="https://gitlab.com/f-santos" target="gitlab.widget" rel="me noopener noreferrer"> <img src="https://img.icons8.com/color/48/000000/gitlab.png" style="width:1em;margin-right:.5em;" alt="GitLab iD icon">GitLab</a> | <a itemprop="sameAs" content="https://orcid.org/0000-0003-1445-3871" href="https://orcid.org/0000-0003-1445-3871" target="orcid.widget" rel="me noopener noreferrer"> <img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon">ORCID</a></center>
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />All the material published in this blog is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</footer</div>
</body>
</html>
