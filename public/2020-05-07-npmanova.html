<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<link rel="alternate"
      type="application/rss+xml"
      href="https://f-santos.gitlab.io/rss.xml"
      title="RSS feed for https://f-santos.gitlab.io/"/>
<title>How to perform a NPMANOVA with R?</title>
<meta name="author" content="Frédéric Santos">
<meta name="referrer" content="no-referrer">
<link href= "solarized-light.css" rel="stylesheet" type="text/css" />
<link rel="icon" href="favicon.ico">
<script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        displayAlign: "center",
        displayIndent: "0em",

        "HTML-CSS": { scale: 100,
                        linebreaks: { automatic: "false" },
                        webFont: "TeX"
                       },
        SVG: {scale: 100,
              linebreaks: { automatic: "false" },
              font: "TeX"},
        NativeMML: {scale: 100},
        TeX: { equationNumbers: {autoNumber: "AMS"},
               MultLineWidth: "85%",
               TagSide: "right",
               TagIndent: ".8em"
             }
});
</script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-AMS_HTML"></script></head>
<body>
<div id="preamble" class="status">
<div class="header"><a href="https://f-santos.gitlab.io/">Frédéric Santos' notebook</a><div class="slogan">Emacs and R tricks for anthropologists and archaeologists.</div><div class="sitelinks"><a href="https://f-santos.gitlab.io/"> Home </a>|<a href="https://f-santos.gitlab.io/about.html"> About </a>|<a href="https://f-santos.gitlab.io/rpackages.html"> R packages </a>|<a href="https://f-santos.gitlab.io/teaching.html"> Teaching (Master BGS) </a>| <div class="dropdown"><a href="https://f-santos.gitlab.io/tags.html" class="dropbtn"> Tags </a><div class="dropdown-content"><a href="https://f-santos.gitlab.io/tag-blog.html">blog</a><a href="https://f-santos.gitlab.io/tag-emacs.html">emacs</a><a href="https://f-santos.gitlab.io/tag-r.html">r</a><a href="https://f-santos.gitlab.io/tag-statistics.html">statistics</a><a href="https://f-santos.gitlab.io/tag-teaching.html">teaching</a></div></div></div></div></div>
<div id="content">
<div class="post-date">07 mai 2020</div><h1 class="post-title"><a href="https://f-santos.gitlab.io/2020-05-07-npmanova.html">How to perform a NPMANOVA with R?</a></h1>
<p>
Performing a non-parametric multivariate analysis of variance (NPMANOVA), also known as permutational multivariate analysis of variance (PERMANOVA), is relatively easy thanks to the function <code>adonis()</code> implemented in the R package <code>vegan</code>. In this blog post :
</p>
<ul class="org-ul">
<li>we&rsquo;ll cover only the <i>one-way</i> NPMANOVA, since more sophisticated extensions are quite rare in archaeological sciences;</li>
<li>we&rsquo;ll assume that the reader is reasonably familiar with the classical univariate analysis of variance (ANOVA).</li>
</ul>

<div id="outline-container-org8198fa0" class="outline-2">
<h2 id="org8198fa0"><span class="section-number-2">1</span> General introduction</h2>
<div class="outline-text-2" id="text-1">
</div>
<div id="outline-container-org22df5af" class="outline-3">
<h3 id="org22df5af"><span class="section-number-3">1.1</span> Goal</h3>
<div class="outline-text-3" id="text-1-1">
<p>
NPMANOVA is a non-parametric multivariate test used to compare the multivariate distribution of several groups [<a href="#anderson2001_NewMethodNonparametric">1</a>,<a href="#anderson2017_PermutationalMultivariateAnalysis">3</a>]. Its null hypothesis is that the centroids of <i>all</i> groups are equal: thus, a rejection of the null (i.e., a low <i>p</i>-value) means that there is at least one pair of groups with significantly different centroids.
</p>

<p>
For example, let&rsquo;s say that you have three groups with \(n_1\), \(n_2\) and \(n_3\) individuals respectively. The individuals are described by continuous variables (e.g., osteometric measurements such as bone lengths or diameters) \(X_1, \cdots, X_p\). The NPMANOVA tests the hypothesis that the three underlying populations have the same &ldquo;profile&rdquo; (i.e., the same centroid, or multivariate mean) for the set of variables under consideration.
</p>

<p>
If this null hypothesis were true, the observed differences among the three centroids should be relatively &ldquo;small&rdquo;. &ldquo;Small&rdquo; means that they should have the same magnitude that what would be obtained under random re-allocation of individuals to one of the three groups. This is exactly how the test proceeds: it is called <i>permutational</i> MANOVA because it simply checks if the differences actually observed are compatible with the differences we get by <i>permuting</i> the group labels of individuals.
</p>
</div>
</div>

<div id="outline-container-orgd8b6d09" class="outline-3">
<h3 id="orgd8b6d09"><span class="section-number-3">1.2</span> (Explicit and implicit) assumptions</h3>
<div class="outline-text-3" id="text-1-2">
<p>
The usual (parametric) MANOVA has relatively restrictive assumptions, that are not always met with the datasets gathered in archaeological sciences. Conversely, the NPMANOVA is a <i>distribution-free</i> method, that makes only one explicit assumption, namely the exchangeability of units under the null hypothesis (i.e., individuals are supposed to be independent, without repeated measures or autocorrelation of any kind).
</p>

<p>
However, several papers [<a href="#anderson2001_NewMethodNonparametric">1</a>,<a href="#anderson2013_PERMANOVAANOSIMMantel">4</a>,<a href="#anderson2017_PermutationalMultivariateAnalysis">3</a>,<a href="#warton2012_DistancebasedMultivariateAnalyses">7</a>] warn that (at least in the case of unbalanced designs, i.e. a different number of individuals in each group), NPMANOVA may lead to spurious results when the groups have a substantially different multivariate dispersion. Indeed, the test statistic may be sensitive to differences in dispersion, so that <i>&ldquo;significant differences may be caused by different within-group variation (dispersion) instead of different mean values of the groups&rdquo;</i> (citation taken from the documentation of the R functions <code>adonis()</code>).
</p>

<p>
Thus, the real (although implicit) assumption is that the groups under comparison have a roughly equal multivariate dispersion, which is similar to the assumption of variance homogeneity in univariate ANOVA. This can be checked either visually with a principal component analysis, or formally with a specific test [<a href="#anderson2006_DistanceBasedTestsHomogeneity">2</a>] implemented in the R function <code>vegan::betadisper()</code>.
</p>
</div>
</div>

<div id="outline-container-org78fd238" class="outline-3">
<h3 id="org78fd238"><span class="section-number-3">1.3</span> A (very) quick overview of the maths</h3>
<div class="outline-text-3" id="text-1-3">
<p>
Full mathematical details can be found in the seminal publication by M. J. Anderson [<a href="#anderson2001_NewMethodNonparametric">1</a>], and a quick version can be found <a href="https://en.wikipedia.org/wiki/Permutational_analysis_of_variance">on Wikipedia</a>. We&rsquo;ll not get into this in this blog post. But, just as a general idea:
</p>
<ul class="org-ul">
<li>The reader may be aware of the general result (used in univariate ANOVA) saying that (in a sample divided in groups of individuals) the total variance is equal to the sum of within-group variance and between-groups variance. NPMANOVA also operates a partition of multivariate dispersion: it is thus another technique of <a href="https://en.wikipedia.org/wiki/Partition_of_sums_of_squares">partition of sum of squares</a>.</li>
<li>The test statistic is a &ldquo;pseudo \(F\) ratio&rdquo;, which is essentially a ratio of between-groups dispersion over within-group dispersion.</li>
<li>Let \(F_\text{obs}\) be the actual \(F\) statistic observed in your data. By performing a great number \(B\) of permutations of the data, we can compute \(B\) values of the \(F\) ratio under the null hypothesis. The (empirical) p-value is thus defined by: \(p = \frac{\#\left\{F \geq F_\text{obs}\right\}+1}{B+1}\), i.e. the proportion of cases where we can get, under the null hypothesis, an \(F\) ratio more extreme than \(F_\text{obs}\).</li>
</ul>

<p>
Remark: the adjustment &ldquo;\(+1\)&rdquo; is the reason why we often choose a number of permutation \(B = 999\) or \(B = 9999\), so that \(B+1\) can be a &ldquo;pleasant&rdquo; number.
</p>
</div>
</div>
</div>

<div id="outline-container-orgbc4b8c6" class="outline-2">
<h2 id="orgbc4b8c6"><span class="section-number-2">2</span> A simple example in R</h2>
<div class="outline-text-2" id="text-2">
<p>
Let&rsquo;s consider a simple example with only two groups and three variables. We&rsquo;ll use the DSP reference sample [<a href="#bruzek2017_ValidationReliabilitySex">5</a>] to illustrate the method. The DSP reference sample is available in the R package <a href="https://gitlab.com/f-santos/anthrostat">anthrostat</a>. More precisely, we will consider the differences between males and females for three pelvic measurements (DCOX, PUM, SPU) within the population sample of Coimbra.
</p>

<p>
This example is rather caricatural since the observed difference between the centroids is very clear, and the sample sizes are important (Figure <a href="#orgd1e29df">1</a>), so that one could think that a NPMANOVA is not really mandatory here. But let&rsquo;s go all the same!
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(anthrostat)
<span style="color: #268bd2; font-weight: bold;">library</span>(scatterplot3d)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Load DSP reference sample:</span>
<span style="color: #268bd2;">data</span>(data_dsp)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Select a subsample (population sample of Coimbra):</span>
samp <span style="color: #268bd2; font-weight: bold;">&lt;-</span> <span style="color: #268bd2;">na.omit</span>(<span style="color: #268bd2;">subset</span>(data_dsp, Collection == <span style="color: #2aa198;">"Coimbra"</span>))
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Display a 3D scatterplot:</span>
<span style="color: #268bd2;">with</span>(samp, <span style="color: #268bd2;">scatterplot3d</span>(x = DCOX, y = SPU, z = PUM,
                         color = <span style="color: #268bd2;">as.numeric</span>(Sex), pch = <span style="color: #b58900;">16</span>,
                         box = <span style="color: #b58900;">FALSE</span>))
</pre>
</div>


<figure id="orgd1e29df">
<img src="2020-05-06-scatterplot3d.png" alt="2020-05-06-scatterplot3d.png">

<figcaption><span class="figure-number">Figure 1: </span>3D scatterplot of males (in red) and females (in black), for the three pelvic measurements DCOX, PUM and SPU, within the population sample of Coimbra.</figcaption>
</figure>

<p>
Performing a NPMANOVA with the R function <code>adonis()</code> requires to choose a <code>method</code>, i.e. a dissimilarity that will be used to calculate pairwise distances between individuals. (This is required to compute the \(F\) ratio.) For most applications in biological anthropology (like here, where we are dealing with continuous variables), the usual euclidean distance should be picked. For more details, one can read the help page of <code>adonis()</code>.
</p>

<p>
Let&rsquo;s perform the NPMANOVA:
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(vegan)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Create the Y matrix of variables under comparison:</span>
Y <span style="color: #268bd2; font-weight: bold;">&lt;-</span> samp[, <span style="color: #268bd2;">c</span>(<span style="color: #2aa198;">"DCOX"</span>, <span style="color: #2aa198;">"SPU"</span>, <span style="color: #2aa198;">"PUM"</span>)]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Perform a one-way NPMANOVA:</span>
<span style="color: #268bd2;">adonis</span>(Y ~ samp$Sex, method = <span style="color: #2aa198;">"euclidean"</span>,
       permutations = <span style="color: #b58900;">999</span>)
</pre>
</div>

<pre class="example">


Call:
adonis(formula = Y ~ samp$Sex, permutations = 999, method = "euclidean") 

Permutation: free
Number of permutations: 999

Terms added sequentially (first to last)

           Df SumsOfSqs MeanSqs F.Model      R2 Pr(&gt;F)    
samp$Sex    1     13527 13527.2  137.51 0.41858  0.001 ***
Residuals 191     18789    98.4         0.58142           
Total     192     32316                 1.00000           
---
Signif. codes:  
0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
</pre>

<p>
Unsurprisingly, the null hypothesis of no differences between males and females is strongly rejected. (Reminder: the <i>p</i>-value is an <i>empirical p</i>-value. With 999 permutations, a <i>p</i>-value of 0.001 is the &ldquo;most significant&rdquo; result we can obtain: that means that the observed difference could be replicated by none of the 999 permutation trials.)
</p>
</div>
</div>

<div id="outline-container-orgda8fbd8" class="outline-2">
<h2 id="orgda8fbd8"><span class="section-number-2">3</span> (Slightly) more advanced topics</h2>
<div class="outline-text-2" id="text-3">
</div>
<div id="outline-container-org12d3650" class="outline-3">
<h3 id="org12d3650"><span class="section-number-3">3.1</span> &ldquo;Should I perform one NPMANOVA or several univariate tests&rdquo;?</h3>
<div class="outline-text-3" id="text-3-1">
<p>
This is not always a trivial decision, and this can even be seen as a controversial choice [<a href="#huberty1989_MultivariateAnalysisMultiple">6</a>].
</p>

<p>
As a reminder, ANOVA tests for differences between three or more groups for <i>one single</i> continuous response variable. Thus, if you have four variables and want to compare three groups, there are essentially two approaches:
</p>
<ol class="org-ol">
<li>You can perform one single MANOVA, comparing the three groups for the considered set of four input variables.</li>
<li>You can perform four different ANOVAs or Kruskal-Wallis tests (one per variable). Warning: in this case, using a method of adjustment of <i>p</i>-values might be necessary if you have a great number of response variables.</li>
</ol>

<p>
In fact, both approaches are not mutually exclusive, and you can perfectly do both of them (although this is part of the controversy detailed in [<a href="#huberty1989_MultivariateAnalysisMultiple">6</a>]). But it is important to note that those two approaches address <i>different research questions</i>: are you interested in differences for each single, isolated outcome variable? or in differences in the whole pattern of multiple dependent variables?
</p>
</div>
</div>

<div id="outline-container-orgbe14864" class="outline-3">
<h3 id="orgbe14864"><span class="section-number-3">3.2</span> How to choose between MANOVA and NPMANOVA?</h3>
<div class="outline-text-3" id="text-3-2">
<p>
The seminal paper by M. J. Anderson [<a href="#anderson2001_NewMethodNonparametric">1</a>] clearly explains the differences between the classical (parametric) MANOVA, and its non-parametric alternative:
</p>
<ul class="org-ul">
<li>NPMANOVA must be used when the (restrictive) assumptions of parametric MANOVA are not satisfied.</li>
<li>But NPMANOVA <i>&ldquo;takes no account of the correlations among variables&rdquo;</i> (see Figure 4 in [<a href="#anderson2001_NewMethodNonparametric">1</a>] for an illustration), so that NPMANOVA is not the appropriate testing framework if you want to know whether the <i>correlation pattern between outcome variables</i> is identical among groups. The classical parametric MANOVA can handle that, but this is not the case of NPMANOVA.</li>
</ul>
</div>

<div id="outline-container-orgb8d9faf" class="outline-4">
<h4 id="orgb8d9faf">Exercise</h4>
<div class="outline-text-4" id="text-orgb8d9faf">
<p>
Visit and read <a href="https://statisticsbyjim.com/anova/multivariate-anova-manova-benefits-use/">this good blog post</a>. In the situation exposed by the author, the differences among the three groups are mostly due to a different correlation pattern between the two response variable. Perform a NPMANOVA on the dataset provided there, and compare the <i>p</i>-value to the <i>p</i>-value obtained with a parametric MANOVA. Do you see how different are those two methods?
</p>
</div>
</div>
</div>

<div id="outline-container-org12f2c72" class="outline-3">
<h3 id="org12f2c72"><span class="section-number-3">3.3</span> Post-hoc analysis</h3>
<div class="outline-text-3" id="text-3-3">
<p>
(Reminder: a NPMANOVA only tests whether <i>all</i> centroids are equal. If your <i>p</i>-value is low, you only know that this hypothesis is very unlikely. The negation of &ldquo;<i>all</i> centroids are equal&rdquo; is simply &ldquo;there exists <i>at least one pair</i> of centroids which are not equal&rdquo;, but you do not know which ones! To investigate this question, one can use <a href="https://statisticsbyjim.com/anova/post-hoc-tests-anova/">post-hoc tests</a>.)
</p>

<p>
To the best of my knowledge, there are not many R functions to perform post-hoc comparisons between groups after a NPMANOVA. The R package <code>pairwiseAdonis</code> (which can be installed <a href="https://github.com/pmartinezarbizu/pairwiseAdonis">from here</a>) seems to be the only solution available (although creating your own procedure of post-hoc tests is not really complicated if you are comfortable with R programming). Its use is documented on the GitHub repo and is quite straightforward.
</p>

<p>
Let&rsquo;s illustrate that on another simple example, making use of the classical <code>iris</code> dataset included in R. We will consider only two response variables: <code>Sepal.Length</code> and <code>Sepal.Width</code>; and we will compare those two outcome variables among three species of iris (50 flowers in each group). First, as usual, one should start with a graphical inspection:
</p>
<div class="org-src-container">
<pre class="src src-R"><span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Load iris dataset:</span>
<span style="color: #268bd2;">data</span>(iris)
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Scatterplot for two variables:</span>
<span style="color: #268bd2; font-weight: bold;">library</span>(lattice)
<span style="color: #268bd2;">xyplot</span>(Sepal.Length ~ Sepal.Width, groups = Species, data = iris,
       pch = <span style="color: #b58900;">16</span>, auto.key = <span style="color: #b58900;">TRUE</span>)
</pre>
</div>


<figure>
<img src="2020-05-07-iris.png" alt="2020-05-07-iris.png">

<figcaption><span class="figure-number">Figure 2: </span>Scatterplot of sepal lengths and widths for three species of iris.</figcaption>
</figure>

<p>
A difference in sepal measurements seems to be clear here (and there is no huge difference in within-group dispersions, so that the assumptions of NPMANOVA are met). Let&rsquo;s verify that with a NPMANOVA:
</p>

<div class="org-src-container">
<pre class="src src-R"><span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Only consider two variables:</span>
Y <span style="color: #268bd2; font-weight: bold;">&lt;-</span> iris[, <span style="color: #268bd2;">c</span>(<span style="color: #2aa198;">"Sepal.Length"</span>, <span style="color: #2aa198;">"Sepal.Width"</span>)]
<span style="color: #93a1a1;">## </span><span style="color: #93a1a1;">Perform the NPMANOVA:</span>
<span style="color: #268bd2;">adonis</span>(Y ~ iris$Species, method = <span style="color: #2aa198;">"euclidean"</span>)
</pre>
</div>

<pre class="example">

Call:
adonis(formula = Y ~ iris$Species, method = "euclidean") 

Permutation: free
Number of permutations: 999

Terms added sequentially (first to last)

              Df SumsOfSqs MeanSqs F.Model      R2 Pr(&gt;F)
iris$Species   2    74.557  37.279  97.999 0.57143  0.001
Residuals    147    55.918   0.380         0.42857       
Total        149   130.475                 1.00000       
                
iris$Species ***
Residuals       
Total           
---
Signif. codes:  
0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
</pre>

<p>
Once again, the very low <i>p</i>-value is a strong evidence against the null hypothesis of no difference between groups. Now let&rsquo;s proceed to the post-hoc analysis:
</p>
<div class="org-src-container">
<pre class="src src-R"><span style="color: #268bd2; font-weight: bold;">library</span>(pairwiseAdonis)
<span style="color: #268bd2;">pairwise.adonis</span>(Y, iris$Species, sim.method = <span style="color: #2aa198;">"euclidean"</span>,
                p.adjust.m = <span style="color: #2aa198;">"bonferroni"</span>)
</pre>
</div>

<pre class="example">

                    pairs Df SumsOfSqs   F.Model        R2
1    setosa vs versicolor  1   32.4466 102.54269 0.5113260
2     setosa vs virginica  1   67.7210 174.47442 0.6403332
3 versicolor vs virginica  1   11.6680  26.72319 0.2142600
  p.value p.adjusted sig
1   0.001      0.003   *
2   0.001      0.003   *
3   0.001      0.003   *
</pre>


<p>
Even when using the most conservative method (<code>"bonferroni"</code>) for the adjustment of <i>p</i>-values, we get <i>for each pair of groups</i> a strong evidence against the hypothesis of no difference.
</p>
</div>
</div>
</div>

<div id="bibliography">
<h2>References</h2>

</div>
<table>

<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="anderson2001_NewMethodNonparametric">1</a>]
</td>
<td class="bibtexitem">
Marti&nbsp;J. Anderson.
 A new method for non-parametric multivariate analysis of variance:
  Non-parametric Anova for ecology.
 <em>Austral Ecology</em>, 26(1):32--46, 2001.
[&nbsp;<a href="http://dx.doi.org/10.1111/j.1442-9993.2001.01070.pp.x">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="anderson2006_DistanceBasedTestsHomogeneity">2</a>]
</td>
<td class="bibtexitem">
Marti&nbsp;J. Anderson.
 Distance-Based Tests for Homogeneity of Multivariate
  Dispersions.
 <em>Biometrics</em>, 62(1):245--253, 2006.
 _eprint:
  https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1541-0420.2005.00440.x.
[&nbsp;<a href="http://dx.doi.org/10.1111/j.1541-0420.2005.00440.x">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="anderson2017_PermutationalMultivariateAnalysis">3</a>]
</td>
<td class="bibtexitem">
Marti&nbsp;J. Anderson.
 Permutational Multivariate Analysis of Variance
  (PERMANOVA).
 In <em>Wiley StatsRef: Statistics Reference Online</em>, pages
  1--15. American Cancer Society, 2017.
 _eprint:
  https://onlinelibrary.wiley.com/doi/pdf/10.1002/9781118445112.stat07841.
[&nbsp;<a href="http://dx.doi.org/10.1002/9781118445112.stat07841">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="anderson2013_PERMANOVAANOSIMMantel">4</a>]
</td>
<td class="bibtexitem">
Marti&nbsp;J. Anderson and Daniel C.&nbsp;I. Walsh.
 PERMANOVA, ANOSIM, and the Mantel test in the face of
  heterogeneous dispersions: What null hypothesis are you testing?
 <em>Ecological Monographs</em>, 83(4):557--574, 2013.
[&nbsp;<a href="http://dx.doi.org/10.1890/12-2010.1">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="bruzek2017_ValidationReliabilitySex">5</a>]
</td>
<td class="bibtexitem">
Jaroslav Br&#367;&#382;ek, Fr&eacute;d&eacute;ric Santos, Bruno Dutailly, Pascal
  Murail, and Eugenia Cunha.
 Validation and reliability of the sex estimation of the human os
  coxae using freely available DSP2 software for bioarchaeology and
  forensic anthropology: BR&#366;&#381;EK et al.
 <em>American Journal of Physical Anthropology</em>, 164(2):440--449,
  2017.
[&nbsp;<a href="http://dx.doi.org/10.1002/ajpa.23282">DOI</a>&nbsp;]

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="huberty1989_MultivariateAnalysisMultiple">6</a>]
</td>
<td class="bibtexitem">
Carl&nbsp;J Huberty and John&nbsp;D Morris.
 Multivariate Analysis Versus Multiple Univariate Analyses.
 <em>Psychological Bulletin</em>, 105(2):302--308, 1989.

</td>
</tr>


<tr valign="top">
<td align="right" class="bibtexnumber">
[<a name="warton2012_DistancebasedMultivariateAnalyses">7</a>]
</td>
<td class="bibtexitem">
David&nbsp;I. Warton, Stephen&nbsp;T. Wright, and Yi&nbsp;Wang.
 Distance-based multivariate analyses confound location and dispersion
  effects.
 <em>Methods in Ecology and Evolution</em>, 3(1):89--101, 2012.
 _eprint:
  https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.2041-210X.2011.00127.x.
[&nbsp;<a href="http://dx.doi.org/10.1111/j.2041-210X.2011.00127.x">DOI</a>&nbsp;]

</td>
</tr>
</table>
<div class="taglist"><a href="https://f-santos.gitlab.io/tags.html">Tags</a>: <a href="https://f-santos.gitlab.io/tag-r.html">R</a> <a href="https://f-santos.gitlab.io/tag-statistics.html">statistics</a> </div></div>
<div id="postamble" class="status"><hr> <footer> <center> <a itemprop="sameAs" content="https://gitlab.com/f-santos" href="https://gitlab.com/f-santos" target="gitlab.widget" rel="me noopener noreferrer"> <img src="https://img.icons8.com/color/48/000000/gitlab.png" style="width:1em;margin-right:.5em;" alt="GitLab iD icon">GitLab</a> | <a itemprop="sameAs" content="https://orcid.org/0000-0003-1445-3871" href="https://orcid.org/0000-0003-1445-3871" target="orcid.widget" rel="me noopener noreferrer"> <img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon">ORCID</a></center>
        <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a><br />All the material published in this blog is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</footer</div>
</body>
</html>
